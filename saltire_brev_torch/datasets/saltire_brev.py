import numpy as np
import pandas as pd
import math
import re
import os
import shutil
import torch
import torchaudio
import zipfile

from astral import LocationInfo, Depression
from astral.sun import dawn, dusk, noon, sunrise, sunset
from conduit.data.datasets.audio.base import CdtAudioDataset
from conduit.data.datasets.utils import AudioTform, UrlFileInfo, download_from_url
from conduit.data.structures import TernarySample
from datetime import datetime
from enum import auto
from functools import cached_property
from pandas.api.types import is_categorical_dtype, is_object_dtype
from pandas import DataFrame, Index
from pathlib import Path
from pytz import timezone
from pykml import parser as kml
from torch import Tensor
from tqdm import tqdm
from typing import (
    Callable,
    ClassVar,
    Dict,
    Final,
    List,
    Optional,
    Tuple,
    Union,
)
from typing_extensions import TypeAlias
from saltire_brev_torch.utils import try_or

__all__ = [
    "SaltireBrev",
]

SampleType: TypeAlias = TernarySample

class SaltireBrev(CdtAudioDataset[SampleType, Tensor, Tensor]):
    """Dataset for audio data collected in various geographic locations."""
    _METADATA_FILENAME: ClassVar[str] = "metadata.parquet"
    _DATA_DIR: ClassVar[str] = "data"

    num_frames_in_segment: int
    _MAX_AUDIO_LEN: Final[int] = 60
    _BITS_PER_BYTE: int = 8
    _AUDIO_SAMPLE_RATE: int = 48_000
    _BIT_RATE: int = 16

    def __init__(
        self,
        root: str,
        *,
        target_attrs: Optional[List[str]] = None,
        sampler: Optional[object] = None,
        transform: Optional[AudioTform] = None,
        download: bool = True,
        segment_len: float = 60,
        sample_rate: int = 48_000,
        reset_index: bool = False,
    ) -> None:
        self.base_dir = Path(root).expanduser()
        self.data_dir = self.base_dir / self._DATA_DIR
        self.download = download
        self.sample_rate = sample_rate
        self.segment_len = segment_len
        self.sampler = sampler

        # extract labels from indices files if first time
        if reset_index or not (self.base_dir / self._METADATA_FILENAME).exists():
            self._extract_metadata()

        # load metadata file
        self.metadata = pd.read_parquet(self.base_dir / self._METADATA_FILENAME)
        self.metadata.index.name = 'file_i'
        # x specifies the files to be loaded by parent dataset class
        x = self.metadata["file_path"].to_numpy()
        # only pass y if available
        if target_attrs is not None:
            y = torch.as_tensor(self._label_encode(self.metadata[target_attrs], inplace=True).to_numpy())
        else:
            y = None

        # add file index as additional tensor
        s = torch.as_tensor(self.metadata.index)

        super().__init__(x=x, y=y, s=s, transform=transform, audio_dir=self.data_dir)

    def load_sample(self, index: int) -> Tensor:
        return self.sampler(self.audio_dir / self.x[index])

    @property
    def segment_len(self) -> float:
        return self._segment_len

    @segment_len.setter
    def segment_len(self, value: float) -> None:
        if value <= 0:
            raise ValueError("Segment length must be positive.")
        value = min(value, self._MAX_AUDIO_LEN)
        self._segment_len = value
        self.num_frames_in_segment = int(self.segment_len * self.sample_rate)

    @property
    def sampler(self) -> Callable:
        return self._sampler

    @sampler.setter
    def sampler(self, sampler: Callable) -> None:
        self._sampler = self._default_sampler if sampler is None else sampler

    @staticmethod
    def _label_encode(
        data: Union[pd.DataFrame, pd.Series],
        inplace=True
    ) -> Union[pd.DataFrame, pd.Series]:
        """label encode the extracted concept/context/superclass information."""
        data = data.copy(deep=not inplace)
        if isinstance(data, pd.Series):
            if is_object_dtype(data) or is_categorical_dtype(data):
                data.update(data.factorize()[0])
                data = data.astype(np.int64)
        else:
            for col in data.columns:
                # add a new column containing the label-encoded data
                if is_object_dtype(data[col]) or is_categorical_dtype(data[col]):
                    data[col] = data[col].factorize()[0]
        return data

    def _download_files(self) -> None:
        """download files from remote"""
        # create necessary directories if they don't already exist.
        self.base_dir.mkdir(parents=True, exist_ok=True)
        # download files from remote
        for finfo in self._FILE_INFO:
            download_from_url(
                file_info=finfo,
                root=self.base_dir,
                logger=self.logger,
                remove_finished=True
            )

    def _extract_metadata(self) -> None:
        """unpack uk and ec metadata and merge into a single master parquet file"""
        self.logger.info("extracting metadata...")

        data = []
        file_regex = re.compile("^(\w+\s\w\s-\s[a-zA-Z]+)_(\d+_\d+).WAV$")
        for file_name in os.listdir(self.data_dir / 'craignish_site_a'):
            site, timestamp = file_regex.search(file_name).groups()
            timestamp = datetime.strptime(timestamp, '%Y%m%d_%H%M%S')
            file_path = str(self.data_dir / 'craignish_site_a' / file_name)
            data.append([file_path, file_name, site, timestamp.hour, timestamp])

        file_regex = re.compile("^([a-zA-Z0-9]+)_\d+.(\d+).WAV$")
        for file_name in os.listdir(self.data_dir / 'sweden'):
            site, timestamp = file_regex.search(file_name).groups()
            timestamp = datetime.strptime(timestamp, '%y%m%d%H%M%S')
            file_path = str(self.data_dir / 'sweden' / file_name)
            data.append([file_path, file_name, site, timestamp.hour, timestamp])

        file_regex = re.compile("^(\w_\d)_[a-zA-Z_]+([0-9-_]+).WAV$")
        for file_name in os.listdir(self.data_dir / 'loch_ryan'):
            site, timestamp = file_regex.search(file_name).groups()
            timestamp = datetime.strptime(timestamp, '%Y-%m-%d_%H-%M-%S')
            file_path = str(self.data_dir / 'loch_ryan' / file_name)
            data.append([file_path, file_name, site, timestamp.hour, timestamp])

        metadata = pd.DataFrame(data=data, columns=['file_path', 'file_name', 'site', 'hour', 'timestamp'])
        metadata.to_parquet(self.base_dir / self._METADATA_FILENAME, engine="auto", index=True)
        self.logger.info("...metadata extracted and indexed")

    def _preprocess_files(self) -> None:
        """segment the waveform files based on :py:attr:`segment_len` and cache the file-segments"""
        if self.segment_len is None:
            return
        processed_audio_dir = self.base_dir / f"segment_len={self.segment_len}"
        processed_audio_dir.mkdir(parents=True, exist_ok=True)

        waveform_paths = self.base_dir / self.metadata["filePath"]
        segment_filenames: List[Tuple[str, str]] = []
        for path in tqdm(waveform_paths, desc="Preprocessing", colour=self._PBAR_COL):
            waveform_filename = path.stem
            waveform, sr = torchaudio.load(path)
            audio_len = waveform.size(-1) / sr
            frac_remainder, num_segments = math.modf(audio_len / self.segment_len)
            num_segments = int(num_segments)

            if frac_remainder >= 0.5:
                self.logger.debug((
                    f"Length of audio file '{path.resolve()}' is not integer-divisible by "
                    f"{self.segment_len}: terminally zero-padding the file along the "
                    "time-axis to compensate."
                ))
                padding = torch.zeros(
                    waveform.size(0),
                    int((self.segment_len - (frac_remainder * self.segment_len)) * sr),
                )
                waveform = torch.cat((waveform, padding), dim=-1)
                num_segments += 1
            if 0 < frac_remainder < 0.5:
                self.logger.debug((
                    f"Length of audio file '{path.resolve()}' is not integer-divisible by"
                    f" {self.segment_len} and not of sufficient length to be padded (fractional"
                    " remainder must be greater than 0.5): discarding terminal segment."
                ))
                waveform = waveform[:, : int(num_segments * self.segment_len * sr)]

            waveform_segments = waveform.chunk(chunks=num_segments, dim=-1)
            for seg_idx, segment in enumerate(waveform_segments):
                segment_filename = f"{waveform_filename}_{seg_idx}.wav"
                segment_filepath = processed_audio_dir / segment_filename
                torchaudio.save(
                    filepath=segment_filepath,
                    src=segment,
                    sample_rate=sr,
                )
                segment_filenames.append((
                    waveform_filename,
                    str(segment_filepath.relative_to(self.base_dir))
                ))

        df = pd.DataFrame(segment_filenames, columns=["file_name", "file_path"])
        df.to_csv(processed_audio_dir / "filepaths.csv", index=False)

    def _default_sampler(self, file_path: str) -> Tensor:
        """resample sample audio using specified sample rate and load a segment"""
        # get metadata first
        metadata = torchaudio.info(file_path)
        # compute number of frames to take with the real sample rate
        num_frames_segment = int(
            self.num_frames_in_segment / self.sample_rate * metadata.sample_rate
        )
        # get random sub-sample
        high = max(1, metadata.num_frames - num_frames_segment)
        frame_offset = torch.randint(low=0, high=high, size=(1,))
        # load segment
        waveform, _ = torchaudio.load(file_path, num_frames=num_frames_segment, frame_offset=frame_offset)
        # resample to correct sample rate
        return torchaudio.functional.resample(
            waveform,
            orig_freq=metadata.sample_rate,
            new_freq=self.sample_rate,
        )

    def _extract_coordinates(self):
        """extract recorder co-ordinate information from kml files"""
        coordinates = {}
        for map_file in self._MAP_FILES:
            document = kml.parse(open(self.maps_dir / map_file, "r"))
            for site in document.getroot().Document.Folder.Placemark:
                site_name = str(site.name).upper()
                coordinates[site_name] = {}
                coords = str(site.Point.coordinates).split(',')
                for metric, coord in zip(['longitude', 'latitude', 'altitude'], coords):
                    coordinates[site_name][metric] = float(coord)
        return coordinates

    def __extract_time_metadata(self, row):
        """extract time metadata from file name"""
        _, _, _, year, month, day, hours, minutes = self._FILE_REGEX.match(row.file_name).groups()
        tzinfo = timezone(self._COUNTRY_TO_TZ[row.country])
        timestamp = tzinfo.localize(datetime(int(year), int(month), int(day), int(hours), int(minutes)))
        info = LocationInfo(row.location, row.country, timestamp, row.latitude, row.longitude)
        return dict(
            timestamp=timestamp,
            hour=int(hours),
            time_at_sunrise=sunrise(info.observer, date=timestamp.date(), tzinfo=timestamp.tzinfo),
            time_at_sunset=sunset(info.observer, date=timestamp.date(), tzinfo=timestamp.tzinfo),
            time_at_astronomical_dawn=try_or(lambda: dawn(info.observer, depression=Depression.ASTRONOMICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_nautical_dawn=try_or(lambda: dawn(info.observer, depression=Depression.NAUTICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_civil_dawn=try_or(lambda: dawn(info.observer, depression=Depression.CIVIL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_astronomical_dusk=try_or(lambda: dusk(info.observer, depression=Depression.ASTRONOMICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_nautical_dusk=try_or(lambda: dusk(info.observer, depression=Depression.NAUTICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_civil_dusk=try_or(lambda: dusk(info.observer, depression=Depression.CIVIL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
        )

