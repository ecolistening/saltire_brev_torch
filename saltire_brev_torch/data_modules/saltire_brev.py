import attr
from conduit.data import BinarySample, TernarySample, TrainValTestSplit
from conduit.data.datasets.utils import CdtDataLoader
from conduit.data.datamodules.audio.base import CdtAudioDataModule
from conduit.types import Stage
from pytorch_lightning.trainer.supporters import CombinedLoader
from torch.nn import Identity
from torch.utils.data import Sampler
from typing import (
    Any,
    List,
    Optional,
    Sequence,
)

from saltire_brev_torch.datasets.saltire_brev import SaltireBrev

__all__ = ["SaltireBrevDataModule"]

@attr.define(kw_only=True)
class SaltireBrevDataModule(CdtAudioDataModule[SaltireBrev]):
    segment_len: float = 60
    sample_rate: int = 48_000
    target_attrs: Optional[List[str]] = None
    sampler: Optional[object] = None

    @staticmethod
    def _batch_converter(batch: TernarySample) -> TernarySample:
        return TernarySample(x=batch.x, y=getattr(batch, "y", None), s=batch.s)

    def make_dataloader(
        self,
        ds: SaltireBrev,
        *,
        batch_size: int,
        shuffle: bool = False,
        drop_last: bool = False,
        batch_sampler: Optional[Sampler[Sequence[int]]] = None,
    ) -> CdtDataLoader[TernarySample]:
        return CdtDataLoader(
            ds,
            batch_size=batch_size if batch_sampler is None else 1,
            shuffle=shuffle,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            drop_last=drop_last,
            persistent_workers=self.persist_workers,
            batch_sampler=batch_sampler,
            converter=self._batch_converter,
        )

    def prepare_data(self, *args: Any, **kwargs: Any) -> None:
        SaltireBrev(
            root=self.root,
            download=True,
            segment_len=self.segment_len,
            target_attrs=self.target_attrs,
        )

    @property
    def train_metadata(self):
        self._train_data_base.metadata

    def predict_dataloader(self):
        eval_train_dataloader = self.make_dataloader(batch_size=self.eval_batch_size, ds=self.train_data)
        return [
            eval_train_dataloader,
            self.val_dataloader(),
            self.test_dataloader()
        ]

    @property
    def _default_transform(self) -> Identity:
        return Identity()

    @property
    def _default_train_transforms(self) -> Identity:
        return Identity()

    @property
    def _default_test_transforms(self) -> Identity:
        return Identity()

    def _get_audio_splits(self) -> TrainValTestSplit[SaltireBrev]:
        data = SaltireBrev(
            root=self.root,
            transform=None,
            sampler=self.sampler,
            segment_len=self.segment_len,
            target_attrs=self.target_attrs,
            sample_rate=self.sample_rate,
            download=False,
        )

        val, test, train = data.random_split(
            props=(self.val_prop, self.test_prop),
            seed=self.seed
        )
        return TrainValTestSplit(train=train, val=val, test=test)
